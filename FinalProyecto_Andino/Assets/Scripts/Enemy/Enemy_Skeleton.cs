using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy_Skeleton : MonoBehaviour
{
    NavMeshAgent navMesh;
    GameObject player;

    bool avistado = false;

    float life = 100;
    float attackInterval;
    bool attacked = false;

    bool dead = false;

    public GameObject lifeBar;
    public Image ImageLife;

    //float destinationx = 0;
    //float destinationz = 0;
    void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        //destinationx = transform.position.x;
        //destinationz = transform.position.z;
    }


    private void Update()
    {
        if(!dead)
        {
            float distance = Vector3.Distance(transform.position, player.transform.position);
            if (distance > 1.5f)
            {
                if (distance < 5 && !avistado)
                {
                    navMesh.SetDestination(player.transform.position);
                    avistado = true;
                }
                if (distance < 15 && avistado)
                {
                    navMesh.SetDestination(player.transform.position);
                }
                else
                {
                    //if(destinationx == transform.position.x && destinationz == transform.position.z)
                    //{
                        avistado = false;
                        //float x = Random.Range(-5f, 5f);
                        //float z = Random.Range(-5f, 5f);
                        //navMesh.SetDestination(new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z));
                        //destinationx = transform.position.x + x;
                    //    //destinationz = transform.position.z + z;
                    //}                   
                    
                }
            }
            if (distance <= 1.5f)
            {
                navMesh.SetDestination(transform.position);
            }

            if (!attacked && attackInterval == 0)
            {
                AttackPlayer();
            }
            ImageLife.fillAmount = life / 100;
            lifeBar.transform.LookAt(player.transform);
        }      
    }

    public void AttackPlayer()
    {
        Vector3 attachPos = transform.position + transform.forward * 0.5f + transform.up * 1.5f;
        foreach (Collider coll in Physics.OverlapSphere(attachPos, 1.5f))
        {
            if (coll.CompareTag("Player"))
            {
                attacked = true;
                coll.GetComponent<PlayerController>().GetHit();
            }
        }
        if(attacked)
        {
            StartCoroutine(AttackInterval());
        }

    }
    public IEnumerator AttackInterval()
    {
        yield return new WaitForSeconds(3);
        attacked = false;
    }

    public IEnumerator DeathInterval()
    {
        yield return new WaitForSeconds(2);
        foreach (SphereCollider sc in transform.GetComponentsInChildren<SphereCollider>())
        {
            sc.enabled = false;
        }
        foreach (CapsuleCollider cc in transform.GetComponentsInChildren<CapsuleCollider>())
        {
            cc.enabled = false;
        }
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }

    public void GetHit()
    {
        if(!dead)
        {
            life -= 20;
            Debug.Log(life);
            if (life == 0)
            {
                foreach (Rigidbody rb in transform.GetComponentsInChildren<Rigidbody>())
                {
                    rb.useGravity = true;
                }
                foreach (SphereCollider sc in transform.GetComponentsInChildren<SphereCollider>())
                {
                    sc.enabled = true;
                }
                foreach (CapsuleCollider cc in transform.GetComponentsInChildren<CapsuleCollider>())
                {
                    cc.enabled = true;
                }
                dead = true;
                lifeBar.gameObject.SetActive(false);
                StartCoroutine(DeathInterval());
            }
        }
    }

}
