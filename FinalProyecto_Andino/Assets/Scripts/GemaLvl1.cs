using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemaLvl1 : MonoBehaviour
{
    public GameObject SpeedActivated;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().rapidezDesplazamientoSprint = 10f;
            SpeedActivated.SetActive(true);
            Destroy(this.gameObject);
        }    
    }
}
