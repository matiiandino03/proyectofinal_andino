using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public InventorySlot[] inventorySlots;
    public GameObject inventoryItemPrefab;

    public static InventoryManager instance;

    public int selectedSlot = -1;
    public GameObject inventory;
    public CameraController cc;

    public delegate void ChangedSelectedItem();
    public ChangedSelectedItem selectedItemChanged;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        ChangeSelectedSlot(0);
    }
    private void Update()
    {
        if(Input.inputString != null)
        {
            bool isNumber = int.TryParse(Input.inputString, out int number);
            if(isNumber && number > 0 && number < 10)
            {
                ChangeSelectedSlot(number - 1);
            }
        }
        if(Input.mouseScrollDelta.y != 0)
        {
            if(selectedSlot + (int)Input.mouseScrollDelta.y < 9 && selectedSlot + (int)Input.mouseScrollDelta.y > -1)
            {
                ChangeSelectedSlot(selectedSlot + (int)Input.mouseScrollDelta.y);
            }
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            inventory.SetActive(!inventory.activeInHierarchy);
            if(inventory.activeInHierarchy)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                Time.timeScale = 0;
                cc.enabled = false;
            }
            if (!inventory.activeInHierarchy)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                Time.timeScale = 1;
                cc.enabled = true;
            }
        }
    }
    void ChangeSelectedSlot(int newValue)
    {
        if(selectedSlot >= 0)
        {
            inventorySlots[selectedSlot].DeSelect();
        }
        inventorySlots[newValue].Select();
        selectedSlot = newValue;
        selectedItemChanged.Invoke();
    }

    //CUANDO NECESITE AGREGAR ITEMS AL INVENTARIO, CUANDO AGARRAS DEL PISO O FARMEAS. LLAMAR A ESTE METODO (CAPAS DSP HAY QUE MODIFICARLO PARA Q SE PUEDA...
    //AGREGAR 16 DE MADERA POR EJEMPLO)
    public bool AddItem(item item)
    {
        for (int i = 0; i < inventorySlots.Length; i++)
        {
            InventorySlot slot = inventorySlots[i];
            InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();
            if (itemInSlot != null && itemInSlot.item == item && itemInSlot.count < 64 && itemInSlot.item.stackeable)
            {
                itemInSlot.count ++;
                itemInSlot.RefereshCount();
                return true;
            }
        }


        for (int i = 0; i < inventorySlots.Length; i++)
        {
            InventorySlot slot = inventorySlots[i];
            InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();    
            if(itemInSlot == null)
            {
                SpawnNewItem(item, slot);
                return true;
            }
        }
        return false;
    }

    void SpawnNewItem(item item, InventorySlot slot)
    {
        GameObject newItemGO = Instantiate(inventoryItemPrefab, slot.transform);
        InventoryItem inventoryItem = newItemGO.GetComponent<InventoryItem>();
        inventoryItem.InitializeItem(item);
    }

    //ESTE LO USAMOS PARA CAMBIAR EL ITEM Q TIENE EN LA MANO POR EJEMPLOOO
    public item GetSelectedItem(bool use)
    {
        InventorySlot slot = inventorySlots[selectedSlot];
        InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();
        if (itemInSlot != null)
        {
            item item = itemInSlot.item;

            if(use)
            {
                itemInSlot.count--;
                if(itemInSlot.count <= 0)
                {
                    Destroy(itemInSlot.gameObject);
                }
                else
                {
                    itemInSlot.RefereshCount();
                }
            }

            return item;
        }
        return null;
    } 
}

