using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Scriptable object/item")]
public class item : ScriptableObject
{
    [Header("Only Gameplay")]
    public Tilemap tile;
    public ItemType type;
    public ActionType actionType;
    public Vector2Int tange = new Vector2Int(5,4);

    [Header("Only UI")]
    public bool stackeable = true;

    [Header("Both")]
    public Sprite image;

    public enum ItemType
    {
        BuildingBlock,Tool,Resource
    }
    public enum ActionType
    {
        Axe,Pickaxe,Sword,none
    }
}
