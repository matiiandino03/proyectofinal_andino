using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsInHud : MonoBehaviour
{
    public item[] Items;
    InventoryManager IM;

    [Header("Resources to Add")]
    public item wood;
    public item stone;
    public item metal;
    [Header("Items Prefabs")]
    public GameObject Sword;
    public GameObject Axe;
    public GameObject Pickaxe;
    public Transform hand;
    GameObject itemInHand;
    void Start()
    {
        IM = InventoryManager.instance;
        IM.selectedItemChanged += SelectedItemChanged;


        //prueba
        InventoryManager.instance.AddItem(Items[0]);
        InventoryManager.instance.AddItem(Items[1]);
        InventoryManager.instance.AddItem(Items[2]);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && itemInHand != null)
        {
            itemInHand.GetComponent<Tool>().Attack();
        }
    }
    public void SelectedItemChanged()
    {
        item item;
        if (IM.inventorySlots[IM.selectedSlot].GetComponentInChildren<InventoryItem>() != null)
        {
            item = IM.inventorySlots[IM.selectedSlot].GetComponentInChildren<InventoryItem>().item;

            if (item.actionType == item.ActionType.Axe)
            {
                Destroy(itemInHand);
                itemInHand = Instantiate(Axe, hand.transform.position, hand.rotation);
                ((Axe)itemInHand.GetComponent<Tool>()).addWood += AddWood;
                itemInHand.transform.parent = hand;
            }
            if (item.actionType == item.ActionType.Sword)
            {
                Destroy(itemInHand);
                Vector3 pos = new Vector3(hand.transform.position.x, hand.transform.position.y - 0.3f, hand.transform.position.z);
                itemInHand = Instantiate(Sword, pos,hand.rotation);               
                itemInHand.transform.parent = hand;
            }
            if (item.actionType == item.ActionType.Pickaxe)
            {
                Destroy(itemInHand);
                itemInHand = Instantiate(Pickaxe, hand.transform.position, hand.rotation);
                ((Pickaxe)itemInHand.GetComponent<Tool>()).addStone += AddStone;
                ((Pickaxe)itemInHand.GetComponent<Tool>()).addMetal += AddMetal;
                itemInHand.transform.parent = hand;
            }
        }       
        else
        {
            Destroy(itemInHand);
        }
    }

    void AddWood()
    {
        IM.AddItem(wood);
        IM.AddItem(wood);
        IM.AddItem(wood);
        IM.AddItem(wood);
    }

    void AddStone()
    {
        IM.AddItem(stone);
        IM.AddItem(stone);
        IM.AddItem(stone);
        IM.AddItem(stone);
    }

    void AddMetal()
    {
        IM.AddItem(metal);
        IM.AddItem(metal);
    }

}
