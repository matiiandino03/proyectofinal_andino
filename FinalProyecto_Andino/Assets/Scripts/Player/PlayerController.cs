using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public float rapidezDesplazamientoSprint = 5.0f;
    public GameObject Jugador;
    public GameObject CheckGround;
    public LayerMask mask;
    public float life = 100;
    public Image lifeBar;
    public Rigidbody rb;
    bool canWalk = true;

    public GameObject panelDamage;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            rapidezDesplazamiento += rapidezDesplazamientoSprint;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = 5.0f;
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
       

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        

        if (Input.GetKeyDown(KeyCode.Space) && Physics.OverlapSphere(CheckGround.transform.position, 1,mask).Length > 0)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 300);
        }

    }

    public void GetHit()
    {
        life -= 10;
        if(life >= 0)
        {
            lifeBar.fillAmount = life / 100;
        }
        if(life == 0)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(0);
        }
        panelDamage.SetActive(true);
        StartCoroutine("Hurt");
    }
   
    IEnumerator Hurt()
    {
        yield return new WaitForSeconds(0.25f);
        panelDamage.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Lava"))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(0);
        }
    }

}
