using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : Tool
{
    public delegate void AddWood();
    public AddWood addWood;
    Animator animator;
    bool canAttack = true;
    public Axe(int typ, int dmg) : base(typ, dmg)
    {
    }
    private void Start()
    {
        animator = gameObject.GetComponentInParent<Animator>();
        canAttack = true;
    }
    //LLamar a este metodo cuando el jugador ataque
    public override void Attack()
    {
        if(canAttack)
        {
            if (Time.timeScale != 0)
            {
                animator = GameObject.FindGameObjectWithTag("hand").GetComponent<Animator>();
                animator.SetBool("Attack", true);

                canAttack = false;
                StartCoroutine("CoolDownAttack");
            }
        }       
    }
    IEnumerator CoolDownAttack()
    {
        yield return new WaitForSeconds(0.30f);
        Vector3 axePos = transform.position + transform.forward * 0.5f + transform.up * 1.5f;
        //Vector3 impactPos;

        foreach (Collider coll in Physics.OverlapSphere(axePos, 1.5f))
        {
            if (coll.CompareTag("Tree"))
            {
                addWood.Invoke();
            }
        }
        yield return new WaitForSeconds(0.30f);
        animator.SetBool("Attack", false);
        canAttack = true;
    }
}
