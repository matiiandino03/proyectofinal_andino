using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickaxe : Tool
{
    public delegate void AddStone();
    public AddStone addStone;

    public delegate void AddMetal();
    public AddMetal addMetal;

    Animator animator;
    bool canAttack = true;
    public Pickaxe(int typ, int dmg) : base(typ, dmg)
    {
    }
    private void Start()
    {
        animator = gameObject.GetComponentInParent<Animator>();
        canAttack = true;
    }

    //LLamar a este metodo cuando el jugador ataque
    public override void Attack()
    {
        if(canAttack)
        {
            if (Time.timeScale != 0)
            {
                animator = GameObject.FindGameObjectWithTag("hand").GetComponent<Animator>();
                animator.SetBool("Attack", true);

                canAttack = false;
                StartCoroutine("CoolDownAttack");
            }
        }
        
    }

    IEnumerator CoolDownAttack()
    {
        yield return new WaitForSeconds(0.30f);
        Vector3 pickaxePos = transform.position + transform.forward * 0.5f + transform.up * 1.5f;

        foreach (Collider coll in Physics.OverlapSphere(pickaxePos, 1.5f))
        {
            if (coll.CompareTag("Stone"))
            {
                addStone.Invoke();
            }
            if (coll.CompareTag("Metal"))
            {
                addMetal.Invoke();
            }
        }
        yield return new WaitForSeconds(0.30f);
        animator.SetBool("Attack", false);
        canAttack = true;
    }
}
