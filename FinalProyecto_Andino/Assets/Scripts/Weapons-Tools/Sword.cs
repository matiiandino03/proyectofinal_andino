using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Tool
{
    Animator animator;
    bool canAttack = true;
    public Sword(int typ, int dmg) : base(typ, dmg)
    {
        animator = gameObject.GetComponentInParent<Animator>();
        canAttack = true;
    }

    private void Start()
    {
        animator = gameObject.GetComponentInParent<Animator>();
        canAttack = true;
    }
    //LLamar a este metodo cuando el jugador ataque
    public override void Attack()
    {
        animator = GameObject.FindGameObjectWithTag("hand").GetComponent<Animator>();
        if (canAttack == true)
        {
            if (Time.timeScale != 0)
            {
                animator.SetBool("Attack", true);
                
                canAttack = false;
                StartCoroutine("CoolDownAttack");
            }
        }
      
    }

    IEnumerator CoolDownAttack()
    {
        yield return new WaitForSeconds(0.30f);
        Vector3 swordPos = transform.position + transform.forward * 0.5f + transform.up * 1.5f;
        foreach (Collider coll in Physics.OverlapSphere(swordPos, 1.5f))
        {
            if (coll.CompareTag("Skeleton"))
            {
                coll.GetComponent<Enemy_Skeleton>().GetHit();
            }
        }
        yield return new WaitForSeconds(0.30f);
        animator.SetBool("Attack", false);
        canAttack = true;
    }


}
