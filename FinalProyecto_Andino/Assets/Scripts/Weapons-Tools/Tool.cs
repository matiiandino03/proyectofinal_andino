using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{
    public int type;
    public int damage;

    public Tool(int typ, int dmg)
    {
        type = typ;
        damage = dmg;
    }

    public virtual void Attack()
    {

    }

    
}
